<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/side', function () {
    return view('side');
});
Route::get('/test', function () {
    return view('testcontent');
});

Auth::routes();

$this->any('logout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index');
Route::post('register/teacher',  'teacher@register');
Route::get('register/teacher', 'teacher@view');
Route::get('/register', 'teacher@reg');

Route::get('/results', 'StudentController@showResult');

//marks input
Route::get('/marks', 'teacher@marks');
Route::post('/marks', 'teacher@mark');
