<?php

namespace App\Http\Controllers;

use App\Score;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
   public function result(){
       if(Auth::user==true){
           return  view('results');
       }
       else{
          redirect('/login');
       }

   }

    //display results to student portal
    public function  showResult(Request $request)
    {
        try {


            $result = new \App\Score();
            $id = Auth::user()->id;

            $result = DB::table('scores')
                ->join('subjects', 'subjects.id', '=', 'scores.subject_id')
                ->select('scores.grade', 'scores.marks', 'subjects.name AS name')
                ->where([['scores.student_id', $id]])
                ->groupBy('subjects.name')
                ->groupBy('scores.grade')
                ->groupBy('scores.marks')
                ->get();

            //$result=DB::table('scores')->where('student_id','=',$id)->get();
            //$result=Score::where('student_id','=',$id)->get();


            //return  view('results');

            if (empty($result)) {
                return view('blank');
            } else {
                return view('results', ['results' => $result]);
                //return view('recordScore',['results'=>$result]);

            }
        }catch (\Exception $exception) {
            //redirect('/login');
        }
    }
        //allow teacher to record marks for every student
      

}
