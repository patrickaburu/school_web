<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   public function testAngular(Request $request){
       $name=$request->input('name');
       $admno=$request->input('admno');

       $student=new Student();
       $student->name=$name;
       $student->admno=$admno;
       $student->class_id=1;
       $student->parent_id=1;
       $student->student_id=1;

       if(empty($student))
       {

       }else{
           return Response::json($student);
       }
   }
}
