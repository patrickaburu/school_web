<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class teacher extends Controller
{
    public function register(request $request){
        //register a new teacher
        $user= new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=bcrypt($request->idno);
        $user->role=3;
        $user->save();

        $teacher = new \App\Teacher();
        $teacher->name=$request->name;
        $teacher->email=$request->email;
        $teacher->idno=$request->idno;
        $teacher->phone=$request->phone;
        $teacher->tsc_no=$request->tsc_no;
        $teacher->user_id=$user->id;
        $teacher->save();


        return redirect('register/teacher')->with('status','Registered Successfuly');

    }
    public function view(){
        return view('teacher.register');
    }

    /**
     *
     */
    public function  test(){
        $list=User::all()->get();
    }
    public function reg(){
        return view('register');
    }
//allow teacher to input marks for every student per subject
    public function marks(Request $request){

        $marks = DB::table('students')
            ->select( 'students.name AS name','students.student_id as student_id')
            ->where([['students.class_id', 1]])
            ->groupBy('students.name')
            ->groupBy('students.student_id')
            ->get();

        return view('recordScore',['marks'=>$marks]);
    }


}
