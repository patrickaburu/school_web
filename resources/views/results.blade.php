@extends('side')
@section('data')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    @section('data')
    </br></br></br></br></br>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Student Results</div>
                    <div class="panel-body">
                        <table class="table w3-bordered w3-border w3-table w3-striped" style="font-size: 12px;">
                            <th><span class="glyphicon glyphicon-tasks"></span> subject</th>
                            <th><span class="glyphicon glyphicon-edit"></span> marks</th>
                            <th><span class="glyphicon glyphicon-map-dollar"></span> grade</th>
                            @foreach($results as $res)
                                <tr>
                                <td>{{$res->name}}</td>
                                <td>{{$res->marks}}%</td>
                              <td>  <span class="blue-text name">{{$res->grade}}</span></td>
                                <tr/>
                                @endforeach

                            <tr>
                                <td></td>
                                <th>Total average Points  <span class="blue-text name">  46 </span></th>
                                <th>Grade  <span class="blue-text name">  C+</span></th>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <th>Position <span class="blue-text name"> 20 out of 54</span></th>
                            </tr>
                        </table>
                        </div>
                    </div>
                </div>
    </div>
    </div>
    @endsection