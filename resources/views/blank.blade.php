
<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">

<nav>
    <div class="image" style="color: white;font-size: 20px">
        <img src="{{url('images/patrick.jpg')}}" width="48" height="48" alt="User" />
        {{ Auth::user()->name }}, {{ Auth::user()->name }}
    </div>
</nav>

<aside id="leftsidebar" class="sidebar"  style="color: black;">
    <!-- User Info -->
    <div class="user-info">
        <div class="image" style="color: white;font-size: 20px">
            {{--<img src="{{url('images/patrick.jpg')}}" width="48" height="48" alt="User" />--}}
            {{--{{ Auth::user()->name }}, {{ Auth::user()->name }}--}}
        </div>
        <div class="info-container" style="color: lightblue">

            <div class="">
                User Type:
                @if(Auth::user()->role==1)
                   Student
                @elseif(Auth::user()->role==2)
                   Parent
                @elseif(Auth::user()->role==3)
                    Teacher
                @elseif(Auth::user()->role==4)
                    Principal
                @endif
               </br> NAME:: {{ Auth::user()->name }}
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>

            @if(Auth::user()->role==2)
                <li>
                    <a href="{{url('records/county/'.Auth::user()->role)}}">
                        View student result </a>
                </li>
                <li>
                    <a href="{{url('records/county/'.Auth::user()->role)}}">
                        fee statements and balance</a>
                </li>
                <li>
                    <a href="{{url('records/county/'.Auth::user()->role)}}">
                        scheduded meetings </a>
                </li>
            @endif


        @if(Auth::user()->role==3)
                <li>
                    <a href="{{url('my-facility/'.Auth::user()->role)}}">
                        <b>register</b> -new student </a>
                </li>
                <li>
                    <a href="{{url('records/facility/'.Auth::user()->role)}}">
                        <b>Enter </b> -Score / marks </a>
                </li>
                <li>
                    <a href="{{url('records/sub-county/'.Auth::user()->role)}}">
                        view list per subject </a>
                </li>
            @endif

            @if(Auth::user()->role==1)
                <li>
                    <a href="{{url('records/county/'.Auth::user()->role)}}">
                       results </a>
                </li>
                <li>
                    <a href="{{url('records/county/'.Auth::user()->role)}}">
                        fee statements </a>
                </li>
                <li>
                    <a href="{{url('records/county/'.Auth::user()->role)}}">
                        assignment </a>
                </li>
            @endif

            @if(Auth::user()->role>=4)
                <li>
                    <a href="{{url('records/country/'.Auth::user()->role)}}">
                        Add new term </a>
                </li>
                <li>
                    <a href="{{url('records/country/'.Auth::user()->role)}}">
                        Add new teacher </a>
                </li>
                <li>
                    <a href="{{url('records/country/'.Auth::user()->role)}}">
                        Add new student </a>
                </li>
                <li>
                    <a href="{{url('records/country/'.Auth::user()->role)}}">
                       view result student </a>
                </li>
                <li>
                    <a href="{{url('records/country/'.Auth::user()->role)}}">
                        view result single student student </a>
                </li>

                {{--<li>--}}
                    {{--<a href="javascript:void(0);" class="menu-toggle">--}}
                        {{--<span>Locations</span>--}}
                    {{--</a>--}}
                    {{--<ul class="ml-menu">--}}
                        {{--<li>--}}
                            {{--<a href="{{url('country')}}">--}}

                                {{--Countries </a>--}}
                        {{--</li>--}}

                        {{--<li>--}}
                            {{--<a href="{{url('county')}}">--}}

                                {{--Counties </a>--}}
                        {{--</li>--}}

                        {{--<li>--}}
                            {{--<a href="{{url('sub-county')}}">--}}

                                {{--Sub-Counties </a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            @endif
        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2017 School.
        </div>
        <div class="version">
            <b>Designed By: </b> <a href="http://www.ptech.co.ke">
                <img src="{{url('images/patrick.jpg')}}" style="max-height: 20px;width: auto">
            </a>
        </div>
    </div>
    <!-- #Footer -->

</aside>

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
<script>$('.button-collapse').sideNav({
                menuWidth: 300, // Default is 240
                closeOnClick: false // Closes side-nav on <a> clicks, useful for Angular/Meteor
            }
    );
    $('.collapsible').collapsible();</script>
