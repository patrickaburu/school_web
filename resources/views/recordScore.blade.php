@extends('side')
@section('data')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    @section('data')
    </br></br></br></br></br>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" >Input Score Per Student   <b>Subject:: Maths  </b> </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/marks') }}">
                            {{ csrf_field() }}

                            <table style="font-size: 12px;">
                            <th><span class="glyphicon glyphicon-tasks"></span> Student</th>
                            <th><span class="glyphicon glyphicon-edit"></span> Score</th>
                            @foreach($marks as $mark)
                                <tr>
                                    <td>{{$mark->name}}</td>
                                    <td>  <div class="col-md-6">
                                            <input id="name" type="number" class="form-control" name="id" value="{{$mark->student_id}}" hidden>

                                        <input id="name" type="number" class="form-control" name="score" required autofocus>
                                        </div>
                                    </td>
                                <tr/>
                            @endforeach
                                <tr>
                                    <td><div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Submit Marks
                                                </button>
                                            </div>
                                        </div></td>
                                </tr>
                        </table>

                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection